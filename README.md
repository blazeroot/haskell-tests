# haskell-tests

* QuickCheck - Property testing
* hspec - Testing framework
* validity - Pre-prepared test/property suite combinators
* quickspec - Property discovery (research)
* easyspec - Practical property discovery (research)

```haskell
-- Some strings
sample (arbitrary :: Gen String)

-- Int bigger than 0
sample ((arbitrary `suchThat` (> 0)) :: Gen Int)

-- Only 1s 3s and 7s
sample (elements [1,3,7] :: Gen Int)

-- To control size: sized resize

```