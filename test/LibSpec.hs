module LibSpec where

import Lib
import Test.Hspec
-- import Test.Hspec.QuickCheck
import Test.QuickCheck

spec :: Spec

spec = do
    describe "rev" $ do
        it "does nothing with the empty list" $ rev [] == ([] :: [Int])

        it "does nothing with a singleton list" $ property $ \e -> rev [e] == [(e :: Int)]

        specify "reversing a list twice gives the original list" $ property $ \xs -> rev (rev xs) == (xs :: [Int])

    -- this is designed to fail
    describe "failing test" $ do
        specify "every number greater than 5 is even" $ forAll (arbitrary `suchThat` (\n -> n > 5)) $ \n -> n > 5 && even (n :: Int)